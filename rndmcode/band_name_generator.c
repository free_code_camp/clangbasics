#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* My simple solution:
int main() {
  char band_name_1[50];
  char band_name_2[50];
  printf("Enter your town:\n");
  scanf("%s", band_name_1);
  printf("Enter a name of your pet:\n");
  scanf("%s", band_name_2);
  printf("Your band name is: %s %s\n", band_name_1, band_name_2);
  return 0;
}
*/

// TODO:
// Add memory management function
// Make the code shorter improving the error handling

// Capitalize the first letter of each word
void capitalize(char *str) {
    if (str[0] != '\0') {
        str[0] = toupper(str[0]);
    }
}

// Check if a string contains only alphabetic characters
int is_alpha(const char *str) {
    for (int i = 0; str[i] != '\0'; i++) {
        if (!isalpha(str[i])) {
            return 0; // Return 0 if any non-letter character is found
        }
    }
    return 1; // Return 1 if all characters are alphabetic
}

int main() {
    // Allocate memory dynamically to save space
    char *town_name = (char *)malloc(50 * sizeof(char));
    char *pet_name = (char *)malloc(50 * sizeof(char));

    // Check if memory allocation was successful
    if (town_name == NULL || pet_name == NULL) {
        fprintf(stderr, "Memory allocation failed.\n");
        return EXIT_FAILURE;
    }

    printf("Enter your town:\n");

    // Using fgets instead of scanf to read the entire line, including spaces
    if (fgets(town_name, 50, stdin) == NULL) {
        fprintf(stderr, "Error reading input.\n");
        free(town_name);
        free(pet_name);
        return EXIT_FAILURE;
    }

    // Remove newline character from fgets input
    town_name[strcspn(town_name, "\n")] = '\0';

    // Validate if the input contains only letters
    if (!is_alpha(town_name)) {
        fprintf(stderr, "Error: Town name should contain only letters.\n");
        free(town_name);
        free(pet_name);
        return EXIT_FAILURE;
    }

    capitalize(town_name);

    printf("Enter the name of your pet:\n");

    if (fgets(pet_name, 50, stdin) == NULL) {
        fprintf(stderr, "Error reading input.\n");
        free(town_name);
        free(pet_name);
        return EXIT_FAILURE;
    }

    pet_name[strcspn(pet_name, "\n")] = '\0';

    // Validate if the input contains only letters
    if (!is_alpha(pet_name)) {
        fprintf(stderr, "Error: Pet name should contain only letters.\n");
        free(town_name);
        free(pet_name);
        return EXIT_FAILURE;
    }

    capitalize(pet_name);

    printf("Your band name is: %s %s\n", town_name, pet_name);

    // Free dynamically allocated memory
    free(town_name);
    free(pet_name);

    return EXIT_SUCCESS;
}
