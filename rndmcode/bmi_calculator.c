#include <stdio.h>

int main() {
    // Declare variables to store height and weight
    float height;
    int weight;

    // Prompt the user to enter their height
    printf("Enter your height (in meters):\n");
    // Read the height input from the user
    scanf("%f", &height);

    // Prompt the user to enter their weight
    printf("Enter your weight (in kilograms):\n");
    // Read the weight input from the user
    scanf("%d", &weight);

    // Calculate the BMI using the formula: weight / (height * height)
    float bmi = weight / (height * height);

    // Print the calculated BMI
    printf("Your BMI is: %.2f\n", bmi);

    // Determine and print the BMI category
    if (bmi < 18.5) {
        printf("You are underweight\n");
    } else if (bmi < 25) {
        printf("You have normal weight\n");
    } else if (bmi < 30) {
        printf("You are overweight\n");
    } else if (bmi < 35) {
        printf("You are obese\n");
    } else {
        printf("You are clinically obese\n");
    }

    return 0;
}

