#include <stdio.h>
#include <windows.h>

// TryHackMe Advent of Code example

void registryCheckTryHackMe() {
  // const char *registryPath =
  //     "HKLM\\Software\\Microsoft\\Windows\\CurrentVersion";
  // const char *valueName = "ProgramFilesDir";
  //
  // // Prepare the command string for reg.exe
  // char command[512];
  // snprintf(command, sizeof(command), "reg query \"%s\" /v %s", registryPath,
  //          valueName);

  // Encoded
  // Encoded PowerShell command to query the registry
  const char *encodedCommand =
      "RwBlAHQALQBJAHQAZQBtAFAAcgBvAHAAZQByAHQAeQAgAC0AUABhAHQAaAAgACIASABLAEwA"
      "TQA6AFwAUwBvAGYAdAB3AGEAcgBlAFwATQBpAGMAcgBvAHMAbwBmAHQAXABXAGkAbgBkAG8A"
      "dwBzAFwAQwB1AHIAcgBlAG4AdABWAGUAcgBzAGkAbwBuACIAIAAtAE4AYQBtAGUAIABQAHIA"
      "bwBnAHIAYQBtAEYAaQBsAGUAcwBEAGkAcgA=";
  // Get-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion" -Name "ProgramFilesDir"

  // Prepare the PowerShell execution command
  char command[512];
  snprintf(command, sizeof(command), "powershell -EncodedCommand %s",
           encodedCommand);

  // Run the command
  int result = system(command);
  // Check for successful execution
  if (result == 0) {
    printf("Registry query executed successfully.\n");
  } else {
    fprintf(stderr, "Failed to execute registry query.\n");
  }
}

// ChatGPT's modern version
void registryCheckChatGPT() {
  const char *registryPath = "Software\\Microsoft\\Windows\\CurrentVersion";
  const char *valueName = "ProgramFilesDir";
  HKEY hKey;
  char value[512];
  DWORD valueSize = sizeof(value);

  // Open the registry key
  if (RegOpenKeyExA(HKEY_LOCAL_MACHINE, registryPath, 0, KEY_READ, &hKey) ==
      ERROR_SUCCESS) {
    printf("Successfully opened registry key.\n");

    // Query the value
    if (RegQueryValueExA(hKey, valueName, NULL, NULL, (LPBYTE)value,
                         &valueSize) == ERROR_SUCCESS) {
      printf("Registry value (%s): %s\n", valueName, value);
    } else {
      fprintf(stderr, "Failed to query registry value: %s\n", valueName);
    }

    // Close the registry key
    RegCloseKey(hKey);
  } else {
    fprintf(stderr, "Failed to open registry key: %s\n", registryPath);
  }
}

int main() {
  printf("Starting registry check...\n");
  // const char *flag = "[REDACTED]";
  registryCheckTryHackMe();
  registryCheckChatGPT();
  return 0;
}
